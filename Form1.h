

#pragma once
#pragma comment(lib,"shell32.lib")
#include <windows.h>
#include <Shellapi.h>

namespace Lab4 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	///
	/// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
	///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
	///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
	///          ������������ �� ������ ��������� �������� � ���������������
	///          ���������, ��������������� ������ �����.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected: 
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog2;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog3;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;

	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::TextBox^  textBox4;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->openFileDialog2 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->openFileDialog3 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button10 = (gcnew System::Windows::Forms::Button());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(110, 98);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(33, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"input:";
			this->label1->Visible = false;
			this->label1->Click += gcnew System::EventHandler(this, &Form1::label1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(110, 128);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(27, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"key:";
			this->label2->Visible = false;
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(149, 95);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 2;
			this->textBox1->Visible = false;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(149, 125);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 3;
			this->textBox2->Visible = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(255, 93);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 4;
			this->button1->Text = L"�����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Visible = false;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(255, 123);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 5;
			this->button2->Text = L"�����";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Visible = false;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(66, 191);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(131, 60);
			this->button3->TabIndex = 6;
			this->button3->Text = L"������������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Visible = false;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(110, 157);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(40, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"output:";
			this->label3->Visible = false;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(149, 154);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 8;
			this->textBox3->Visible = false;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(255, 152);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 9;
			this->button4->Text = L"�����";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Visible = false;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog1_FileOk);
			// 
			// openFileDialog2
			// 
			this->openFileDialog2->FileName = L"openFileDialog2";
			this->openFileDialog2->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog2_FileOk);
			// 
			// openFileDialog3
			// 
			this->openFileDialog3->FileName = L"openFileDialog3";
			this->openFileDialog3->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog3_FileOk);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(12, 12);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(75, 62);
			this->button5->TabIndex = 10;
			this->button5->Tag = L"";
			this->button5->Text = L"������";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(122, 12);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(75, 62);
			this->button6->TabIndex = 11;
			this->button6->Text = L"�������";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(230, 12);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(75, 62);
			this->button7->TabIndex = 12;
			this->button7->Text = L"DES";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(338, 13);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(75, 61);
			this->button9->TabIndex = 14;
			this->button9->Text = L"�������������";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &Form1::button9_Click);
			// 
			// button10
			// 
			this->button10->Location = System::Drawing::Point(230, 191);
			this->button10->Name = L"button10";
			this->button10->Size = System::Drawing::Size(138, 60);
			this->button10->TabIndex = 15;
			this->button10->Text = L"�������������";
			this->button10->UseVisualStyleBackColor = true;
			this->button10->Visible = false;
			this->button10->Click += gcnew System::EventHandler(this, &Form1::button10_Click);
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(149, 95);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 16;
			this->textBox4->Visible = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(428, 273);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->button10);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				 openFileDialog1->ShowDialog();
			 }
private: System::Void openFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 textBox1->Text = openFileDialog1->FileName;
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog2->ShowDialog();
		 }
private: System::Void openFileDialog2_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 textBox2->Text = openFileDialog2->FileName;
		 }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
			 openFileDialog3->ShowDialog();
		 }
private: System::Void openFileDialog3_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 textBox3->Text = openFileDialog3->FileName;
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (String::IsNullOrEmpty(textBox1->Text) || String::IsNullOrEmpty(textBox3->Text) || (String::IsNullOrEmpty(textBox2->Text) && (textBox4->Text->CompareTo("4") != 0)))
				 MessageBox::Show("������� ������� ���������!");
			 else
			 {
				 if (textBox4->Text->CompareTo("1") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 strcat(itog, " code");
					 ShellExecuteA(0, "open", "Caesar.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("2") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 strcat(itog, " code");
					 ShellExecuteA(0, "open", "Vigenere.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("3") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 ShellExecuteA(0, "open", "DESSSS.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("4") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, "c ");
					 strcat(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 ShellExecuteA(0, "open", "BMP_read.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
			 }
		 }
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
			 textBox4->Text = "3";
			 label1->Visible = true;
			 label2->Visible = true;
			 label1->Text = "Input:";
			 label2->Text = "Key:";
			 label3->Visible = true;
			 textBox1->Visible = true;
			 textBox1->Text = "";
			 textBox2->Visible = true;
			 textBox2->Text = "";
			 textBox3->Visible = true;
			 textBox3->Text = "";
			 button1->Visible = true;
			 button2->Visible = true;
			 button3->Visible = true;
			 button4->Visible = true;
			 button10->Visible = true;
		 }
private: System::Void button10_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (String::IsNullOrEmpty(textBox1->Text) || String::IsNullOrEmpty(textBox3->Text) || (String::IsNullOrEmpty(textBox2->Text) && (textBox4->Text->CompareTo("4") != 0)))
				 MessageBox::Show("������� ������� ���������!");
			 else
			 {
				 if (textBox4->Text->CompareTo("1") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 strcat(itog, " decode");
					 ShellExecuteA(0, "open", "Caesar.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("2") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 strcat(itog, " decode");
					 ShellExecuteA(0, "open", "Vigenere.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("3") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr2);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 ShellExecuteA(0, "open", "DESDEC.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
				 if (textBox4->Text->CompareTo("4") == 0)
				 {
					 const char* ptr1 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox1->Text);
					 const char* ptr2 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox2->Text);
					 const char* ptr3 = (const char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBox3->Text);
					 char itog[300];
					 strcpy(itog, "d ");
					 strcat(itog, ptr1);
					 strcat(itog, " ");
					 strcat(itog, ptr3);
					 ShellExecuteA(0, "open", "BMP_read.exe", (LPCSTR) itog, NULL, SW_SHOWNORMAL);
				 }
			 }
		 }
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
			 textBox4->Text = "1";
			 label1->Visible = true;
			 label1->Text = "Input:";
			 label2->Text = "Shift:";
			 label2->Visible = true;
			 label3->Visible = true;
			 textBox1->Visible = true;
			 textBox1->Text = "";
			 textBox2->Visible = true;
			 textBox2->Text = "";
			 textBox3->Visible = true;
			 textBox3->Text = "";
			 button1->Visible = true;
			 button2->Visible = false;
			 button3->Visible = true;
			 button4->Visible = true;
			 button10->Visible = true;
		 }
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
			 textBox4->Text = "2";
			 label1->Visible = true;
			 label1->Text = "Input:";
			 label2->Text = "Key:";
			 label2->Visible = true;
			 label3->Visible = true;
			 textBox1->Visible = true;
			 textBox1->Text = "";
			 textBox2->Visible = true;
			 textBox2->Text = "";
			 textBox3->Visible = true;
			 textBox3->Text = "";
			 button1->Visible = true;
			 button2->Visible = true;
			 button3->Visible = true;
			 button4->Visible = true;
			 button10->Visible = true;
		 }
private: System::Void button9_Click(System::Object^  sender, System::EventArgs^  e) {
			 textBox4->Text = "4";
			 label1->Visible = true;
			 label1->Text = "Picture:";
			 label2->Text = "Info:";
			 label2->Visible = true;
			 label3->Visible = true;
			 textBox1->Visible = true;
			 textBox1->Text = "";
			 textBox2->Visible = true;
			 textBox2->Text = "";
			 textBox3->Visible = true;
			 textBox3->Text = "";
			 button1->Visible = true;
			 button2->Visible = true;
			 button3->Visible = true;
			 button4->Visible = true;
			 button10->Visible = true;
		 }
};
}

